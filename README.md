# Currency Converter App - Android

## Descripción

La Currency Converter App es una aplicación desarrollada en Kotlin que permite a los usuarios realizar conversiones de monedas de manera rápida y sencilla en dispositivos Android. Con una interfaz intuitiva y fácil de usar, esta aplicación proporciona información actualizada sobre tasas de cambio y permite a los usuarios convertir entre diferentes monedas de manera eficiente.

## Características Principales

1. **Conversión Rápida:** Realiza conversiones de moneda de forma instantánea con tasas de cambio en tiempo real.

2. **Amplia Variedad de Monedas:** Admite una amplia gama de monedas internacionales para satisfacer las necesidades de los usuarios.

3. **Interfaz Intuitiva:** Diseño simple y fácil de usar para una experiencia de usuario sin complicaciones.

4. **Actualización Automática de Tasas de Cambio:** Las tasas de cambio se actualizan automáticamente para garantizar datos precisos y actuales.

5. **Historial de Conversiones:** Registra el historial de las conversiones realizadas para referencia futura.

