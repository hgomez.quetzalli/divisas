package com.santa.ana.divisas.di

import com.santa.ana.divisas.BASE_URL
import com.santa.ana.divisas.api.ApiService
import com.santa.ana.divisas.api.ApiServiceInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * We use this object to do retrofit dependency injection
 */

@Module
@InstallIn(SingletonComponent::class)
object ApiServiceModule {

    @Provides
    fun provideApiService(retrofit: Retrofit) : ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        return retrofit
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient{
        val okHttpClient = OkHttpClient.Builder().addInterceptor(ApiServiceInterceptor).build()
        return okHttpClient
    }

}