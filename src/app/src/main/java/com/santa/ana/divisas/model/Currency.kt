package com.santa.ana.divisas.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Currency(val name: String, val nameCurrency: String, var amount:String): Parcelable