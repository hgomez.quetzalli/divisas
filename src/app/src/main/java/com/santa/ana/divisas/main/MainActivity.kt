package com.santa.ana.divisas.main

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.android.material.textfield.TextInputEditText
import com.santa.ana.divisas.Preference
import com.santa.ana.divisas.R
import com.santa.ana.divisas.databinding.ActivityMainBinding
import com.santa.ana.divisas.delimiter
import com.santa.ana.divisas.favorites.AddCurrencyActivity
import com.santa.ana.divisas.graphics.GraphicsActivity
import com.santa.ana.divisas.main.listView.CurrencyAdapter
import com.santa.ana.divisas.model.Currency
import com.santa.ana.divisas.model.SignsCountries
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private lateinit var currency1: String
    private lateinit var currency2: String
    private lateinit var amount1: String
    private lateinit var amount2: String
    private lateinit var listCurrencies: String
    private var requestCurrency = 0
    private val kindDialog = "dialog"
    private val genericAmount = "1"
    private lateinit var signsCountries: SignsCountries
    lateinit var mAdView : AdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //we get the preference
        val prefs = Preference.getPreference(this)
        currency1 = prefs!!.currency1
        currency2 = prefs.currency2
        amount1 = prefs.amount1
        amount2 = prefs.amount2
        listCurrencies = prefs.listCurrencies

        MobileAds.initialize(this) {}
        mAdView = findViewById(R.id.adView)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        //we make the call to get all the codes
        mainViewModel.getAllCode()

        //we make the adapter instance and we give the adapter to the recycler
        val recycler = binding.currenciesRecycler
        recycler.layoutManager = GridLayoutManager(applicationContext, 1)
        val adapter = CurrencyAdapter()
        adapter.setOnItemClickListener {
            Toast.makeText(
                applicationContext,
                getString(R.string.country_currency_format, it.nameCurrency, it.name),
                Toast.LENGTH_SHORT
            ).show()
        }
        recycler.adapter = adapter

        //we observe when the list of conversions changes
        mainViewModel.conversionList.observe(this) { convercion ->

            /**
             * When requestCurrency is zero the request comes from countryCode or dialog
             *
             * when requestCurrency is one the request comes from autoEditText1. It means, we change the value of autoEditText1 for another currency
             *
             * when requestCurrency is two the request comes from autoEditText2. It means, we change the value of autoEditText2 for another currency
             */
            if (requestCurrency == 0) {

                //conversion.base is equals currency1 it is because the request comes from the observe countryCode or from editText1
                //otherwise the request comes from editText2
                if (convercion.base == currency1) {
                    binding.idTextInputText1.text = convercion.amount.toString()
                    binding.idTextInputText2.text = convercion.rates[currency2]
                    amount1 = convercion.amount.toString()
                    amount2 = convercion.rates[currency2].toString()
                } else {
                    binding.idTextInputText2.text = convercion.amount.toString()
                    binding.idTextInputText1.text = convercion.rates[currency1]
                    amount1 = convercion.rates[currency1].toString()
                    amount2 = convercion.amount.toString()
                }
                //we save the attributes in preferences
                Preference.setPreference(
                    this,
                    Preference(amount1, amount2, currency1, currency2, listCurrencies)
                )
            } else if (requestCurrency == 1) {
                /**
                 * the value of currency1 was to changed and now it is the same as currency2.
                 * I have two options for the format of the response
                 */
                if (currency1 == currency2) {
                    binding.idTextInputText1.text = genericAmount
                    binding.idTextInputText2.text = genericAmount
                    amount1 = genericAmount
                    amount2 = genericAmount
                } else {
                    binding.idTextInputText2.text = convercion.rates[currency2]
                    amount2 = convercion.rates[currency2].toString()
                    Preference.setPreference(
                        this,
                        Preference(
                            amount1,
                            convercion.rates[currency2].toString(),
                            convercion.base,
                            currency2,
                            listCurrencies
                        )
                    )
                }

            } else if (requestCurrency == 2) {
                /**
                 * the value of currency2 was to changed and now it is the same as currency1.
                 * I have two options for the format of the response
                 */
                if (currency1 == currency2) {
                    binding.idTextInputText1.text = genericAmount
                    binding.idTextInputText2.text = genericAmount
                    amount1 = genericAmount
                    amount2 = genericAmount
                } else {
                    binding.idTextInputText1.text = convercion.rates[currency1]
                    amount1 = convercion.rates[currency1].toString()
                    Preference.setPreference(
                        this,
                        Preference(
                            convercion.rates[currency1].toString(),
                            amount2,
                            currency1,
                            convercion.base,
                            listCurrencies
                        )
                    )
                }

            }
            //we return requestCurrency to zero because zero is the default value
            requestCurrency = 0

            val listCurrency = mutableListOf<Currency>()
            /**
             * We separate the string with the list of currencies that we have in preferences.
             * We loop through the list we got by splitting the string
             * If there is a match between the list value and the response value
             * we add an instance of currency to the list
             */
            val myCountries = listCurrencies.split(delimiter)
            var i = 0
            while (i < myCountries.size) {
                if (signsCountries.sign[myCountries[i].trim()] != null) {
                    listCurrency.add(
                        Currency(
                            signsCountries.sign[myCountries[i].trim()]!!,
                            myCountries[i].trim(),
                            ""
                        )
                    )
                }
                i += 1
            }

            /**
             * we loop through the list of currency and when the value of the list is equal to currency1 we change the amount of currency we have to amount1.
             * it is the same for currency2
             * when the value of the list is different from currency1 and currency2 we look for the value in the response of the service
             */
            i = 0
            while (i < listCurrency.size) {
                when (listCurrency[i].nameCurrency) {
                    currency1 -> {
                        listCurrency[i].amount = amount1
                    }
                    currency2 -> {
                        listCurrency[i].amount = amount2
                    }
                    else -> {
                        listCurrency[i].amount = convercion.rates[listCurrency[i].nameCurrency]!!
                    }
                }
                i += 1
            }

            //we give the list to the adapter
            adapter.submitList(listCurrency)


        }

        //Observe for countryCode service
        mainViewModel.countryCode.observe(this) { mapCountry ->

            if(mapCountry!=null){
                this.signsCountries = SignsCountries(mapCountry)
            }

            /**
             * we create a String list with the name of the country and its code
             */
            val list = mutableListOf<String>()
            val setKeys = mapCountry?.keys
            if (setKeys != null) {
                var i = 0
                while (i < setKeys.size) {
                    list.add(
                        getString(
                            R.string.country_currency_format,
                            mapCountry[setKeys.elementAt(i)],
                            setKeys.elementAt(i)
                        )
                    )
                    i += 1
                }
            }

            //i give a country and a code to autoEditText1
            binding.idAutoCompleteText1.setText(
                getString(
                    R.string.country_currency_format,
                    mapCountry?.get(currency1),
                    currency1
                )
            )
            //i give a country and a code to autoEditText2
            binding.idAutoCompleteText2.setText(
                getString(
                    R.string.country_currency_format,
                    mapCountry?.get(currency2),
                    currency2
                )
            )

            //i give the list to the adapter and i give the adapter to AutoEditText1 and AutoEditText2
            val adapter = ArrayAdapter(applicationContext, R.layout.list_currencies_item, list)
            binding.idAutoCompleteText1.setAdapter(adapter)
            binding.idAutoCompleteText2.setAdapter(adapter)

            //i give amount1 to editText1. Amount1 was in preferences
            //i call the service to get the values of the currencies
            if (amount1.isNotEmpty()) {
                binding.idTextInputText1.text = amount1
                mainViewModel.getConversion(amount1, currency1)
            }

        }

        //when i press editText1 it opens a dialog where i put the new value to call the service that gives us the conversion
        binding.idTextInputText1.setOnClickListener {
            val startDialogFragment = StartDialogFragment(mainViewModel, currency1)
            startDialogFragment.show(supportFragmentManager, kindDialog)
        }

        //when i press editText2 it opens a dialog where i put the new value to call the service that gives us the conversion
        binding.idTextInputText2.setOnClickListener {
            val startDialogFragment = StartDialogFragment(mainViewModel, currency2)
            startDialogFragment.show(supportFragmentManager, kindDialog)
        }

        /**
         * When i change the value of autoEditText1 i call the conversion service with the new values
         */
        binding.idAutoCompleteText1.onItemClickListener =
            OnItemClickListener { parent, _, position, _ ->
                requestCurrency = 1
                val currencySelected = parent.getItemAtPosition(position) as String
                val split = currencySelected.split(delimiter)
                currency1 = split[1].trim()
                mainViewModel.getConversion(amount1, currency1)
            }

        /**
         * When i change the value of autoEditText2 i call the conversion service with the new values
         */
        binding.idAutoCompleteText2.onItemClickListener =
            OnItemClickListener { parent, _, position, _ ->
                requestCurrency = 2
                val currencySelected = parent.getItemAtPosition(position) as String
                val split = currencySelected.split(delimiter)
                currency2 = split[1].trim()
                mainViewModel.getConversion(amount2, currency2)
            }

        binding.idFloatingBtnGraphics.setOnClickListener {
            val intent = Intent(applicationContext, GraphicsActivity::class.java)
            intent.putExtra(GraphicsActivity.COUNTRIES_KEY, currency1+delimiter+currency2)
            startActivity(intent)
        }

        binding.idFloatingBtnAdd.setOnClickListener {
            val intent = Intent(applicationContext, AddCurrencyActivity::class.java)
            intent.putExtra(AddCurrencyActivity.COUNTRIES_KEY, currency1+ delimiter+currency2)
            intent.putExtra(AddCurrencyActivity.AMOUNT_KEY, amount1+ delimiter+amount2)
            intent.putExtra(AddCurrencyActivity.CURRENCIES_KEY, listCurrencies)
            intent.putExtra(AddCurrencyActivity.MAP_KEY, signsCountries)
            startActivity(intent)
        }
    }

    /**
     * class to create a dialog
     */
    class StartDialogFragment(
        private val mainViewModel: MainViewModel,
        private val currency: String
    ) : DialogFragment() {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return activity?.let {
                val builder = AlertDialog.Builder(it)
                // Get the layout inflater
                val inflater = requireActivity().layoutInflater
                val view = inflater.inflate(R.layout.alert_dialog_item, null)
                // Inflate and set the layout for the dialog
                // Pass null as the parent view because its going in the dialog layout
                builder.setView(view)
                    // Add action buttons
                    .setPositiveButton(getString(R.string.accept)) { _, _ ->
                        //i call the conversion service with the editText value of the dialog
                        val editText =
                            view.findViewById<TextInputEditText>(R.id.idTextInputTextAlert)
                        mainViewModel.getConversion(editText.text.toString(), currency)
                    }
                    .setNegativeButton(getString(R.string.cancel)) { _, _ ->
                        dialog?.cancel()
                    }
                builder.create()
            } ?: throw IllegalStateException("Activity cannot be null")
        }
    }
}