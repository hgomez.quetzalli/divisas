package com.santa.ana.divisas.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TimeSeries (val amount: Double, val base: String, val start_date: String,
                  val end_date:String, val rates: Map<String, Map<String, String> >) : Parcelable