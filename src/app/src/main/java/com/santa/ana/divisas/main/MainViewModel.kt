package com.santa.ana.divisas.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santa.ana.divisas.api.ApiResponseStatus
import com.santa.ana.divisas.model.Conversion
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: MainTasks) : ViewModel(){

    /**
     * we share the liveData with the MainActivity, it has a map where the key is the country and
     * the value is the country code
     */
    private val _countryList = MutableLiveData<Map<String, String>?>()
    val countryCode : LiveData<Map<String, String>?>
        get() = _countryList

    /**
     * we are going to use this liveData to the progressBar
     */
    private val _status = MutableLiveData<ApiResponseStatus<Any>>()
    val status : LiveData<ApiResponseStatus<Any>>
        get() = _status

    /**
     * we share the liveData with the MainActivity, it has a Conversion class, t
     * his class has the amount of all currencies
     */
    private val _conversionList = MutableLiveData<Conversion>()
    val conversionList : LiveData<Conversion>
        get() = _conversionList

    /**
     * function that request the amount of all currencies
     */
    fun getConversion(amount: String, from : String){
        viewModelScope.launch {
            _status.value = ApiResponseStatus.Loading()
            handleResponseStatusConversion(mainRepository.getConversion(amount, from))
        }
    }

    /**
     * function that request all country codes
     */
    fun getAllCode(){
        viewModelScope.launch {
            _status.value = ApiResponseStatus.Loading()
            handleResponseStatus(mainRepository.getAllCode())
        }
    }

    /**
     * function that handles the status of the response and the values of the getAllCode function
     */
    @Suppress("UNCHECKED_CAST")
    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<Map<String, String>>){
        if(apiResponseStatus is ApiResponseStatus.Success){
            _countryList.value = apiResponseStatus.data!!
        }else if(apiResponseStatus is ApiResponseStatus.Loading){
            Log.d("logDivisas", "mainviewmodel handle loading")
        }else if(apiResponseStatus is ApiResponseStatus.Error){
            Log.d("logDivisas", "mainviewmodel handle error")
        }
        _status.value = apiResponseStatus as ApiResponseStatus<Any>
    }

    /**
     * function that handles the status of the response and the values of the getConversion function
     */
    @Suppress("UNCHECKED_CAST")
    private fun handleResponseStatusConversion(apiResponseStatus: ApiResponseStatus<Conversion>){
        if(apiResponseStatus is ApiResponseStatus.Success){
            _conversionList.value = apiResponseStatus.data!!
        }else if(apiResponseStatus is ApiResponseStatus.Loading){
            Log.d("logDivisas", "mainviewmodel handle loading")
        }else if(apiResponseStatus is ApiResponseStatus.Error){
            Log.d("logDivisas", "mainviewmodel handle error")
        }
        _status.value = apiResponseStatus as ApiResponseStatus<Any>
    }
}