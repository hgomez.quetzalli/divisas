package com.santa.ana.divisas.api.responses

data class TimeSeriesResponse (val amount: Double, val base: String, val start_date: String,
                               val end_date:String, val rates: Map<String, Map<String, String> >)