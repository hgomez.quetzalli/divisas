package com.santa.ana.divisas.graphics

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santa.ana.divisas.api.ApiResponseStatus
import com.santa.ana.divisas.model.TimeSeries
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel class for handling graphics related data.
 *
 * @param graphicsRepository The repository to fetch data from.
 */
@HiltViewModel
class GraphicsViewModel @Inject constructor(private val graphicsRepository: GraphicsRepository) : ViewModel() {

    /**
     * LiveData object for holding the list of time series data.
     */
    private val _listTimeSeries = MutableLiveData<TimeSeries>()
    val listTimeSeries : LiveData<TimeSeries>
        get() = _listTimeSeries

    /**
     * LiveData object for holding the API response status.
     */
    private val _status = MutableLiveData<ApiResponseStatus<Any>>()
    val status : LiveData<ApiResponseStatus<Any>>
        get() = _status

    /**
     * Fetches the time series data from the repository.
     *
     * @param time The time interval for which to fetch the data.
     * @param from a string representing the main currency
     * @param to a string representing the secondary currency
     */
    fun getTimeSeries(time: String, from: String, to: String){
        viewModelScope.launch {
            _status.value = ApiResponseStatus.Loading()
            handleResponseStatus(graphicsRepository.getTimeSeries(time, from, to))
        }
    }

    /**
     * Handles the API response status and updates the corresponding LiveData object.
     *
     * @param apiResponseStatus The API response status.
     */
    @Suppress("UNCHECKED_CAST")
    private fun handleResponseStatus(apiResponseStatus: ApiResponseStatus<TimeSeries>){
        if(apiResponseStatus is ApiResponseStatus.Success){
            _listTimeSeries.value = apiResponseStatus.data!!
        }else if(apiResponseStatus is ApiResponseStatus.Loading){
            Log.d("logDivisas", "mainviewmodel handle loading")
        }else if(apiResponseStatus is ApiResponseStatus.Error){
            Log.d("logDivisas", "mainviewmodel handle error")
        }
        _status.value = apiResponseStatus as ApiResponseStatus<Any>
    }
}