package com.santa.ana.divisas

const val BASE_URL = "https://api.frankfurter.app/"
const val GET_ALL_CODE = "currencies"
const val GET_CONVERSION = "latest"

const val delimiter = "-"

const val LIST_CURRENCIES = "CAD-CNY-EUR-JPY"

