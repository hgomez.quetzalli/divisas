package com.santa.ana.divisas.api

import com.santa.ana.divisas.GET_ALL_CODE
import com.santa.ana.divisas.GET_CONVERSION
import com.santa.ana.divisas.api.responses.ConversionResponse
import com.santa.ana.divisas.api.responses.TimeSeriesResponse

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

lateinit var time: String
interface ApiService{

    @GET(GET_ALL_CODE)
    suspend fun getAllCode() : Map<String, String>

    @GET(GET_CONVERSION)
    suspend fun getConversion(@Query("amount") amount : String, @Query("from") from : String) : ConversionResponse

    @GET("{time}")
    suspend fun getTimeSeries(@Path("time") time: String, @Query("from") amount : String, @Query("to") from : String) : TimeSeriesResponse


}
