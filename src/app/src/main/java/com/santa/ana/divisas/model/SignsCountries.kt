package com.santa.ana.divisas.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class SignsCountries (val sign: Map<String, String>): Parcelable