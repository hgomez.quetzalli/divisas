package com.santa.ana.divisas

import android.app.Activity
import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * A data class representing user preferences for currency conversion.
 *
 * @param amount1 the amount of the first currency to convert
 * @param amount2 the amount of the second currency to convert
 * @param currency1 the code of the first currency to convert
 * @param currency2 the code of the second currency to convert
 * @param listCurrencies a string containing the list of currencies
 */
@Parcelize
class Preference (val amount1: String,
                  val amount2: String,
                  val currency1: String,
                  val currency2: String,
                  val listCurrencies: String) : Parcelable {

    companion object{

        /**
         * The name of the preference where the user's preferences are stored.
         */
        private const val PREF_PREFS = "preferencia"

        /**
         * The keys used to store and retrieve the values of the preference fields.
         */
        private const val AMOUNT_1_KEY = "amount1"
        private const val AMOUNT_2_KEY = "amount2"
        private const val CURRENCY_1_KEY = "currency1"
        private const val CURRENCY_2_KEY = "currency2"
        private const val LIST_CURRENCIES_KEY = "listCurrencies"

        /**
         * Saves the given [preference] to the shared preferences of the given [activity].
         *
         * @param activity the activity where the preferences are stored
         * @param preference the preference to save
         */
        fun setPreference(activity: Activity, preference: Preference){
            activity.getSharedPreferences(PREF_PREFS, Context.MODE_PRIVATE).also {
                    sharedPreferences ->
                sharedPreferences.edit()
                    .putString(AMOUNT_1_KEY, preference.amount1)
                    .putString(AMOUNT_2_KEY, preference.amount2)
                    .putString(CURRENCY_1_KEY, preference.currency1)
                    .putString(CURRENCY_2_KEY, preference.currency2)
                    .putString(LIST_CURRENCIES_KEY, preference.listCurrencies).apply()
            }
        }

        /**
         * Returns the user's saved preferences from the shared preferences of the given [activity].
         * If the preferences are not found, returns `null`.
         *
         * @param activity the activity where the preferences are stored
         * @return the user's saved preferences, or `null` if not found
         */
        fun getPreference(activity: Activity): Preference? {

            val prefs =
                activity.getSharedPreferences(PREF_PREFS, Context.MODE_PRIVATE) ?: return null

            return Preference(
                prefs.getString(AMOUNT_1_KEY, "1") ?: "",
                prefs.getString(AMOUNT_2_KEY, "1") ?: "",
                prefs.getString(CURRENCY_1_KEY, "MXN") ?: "",
                prefs.getString(CURRENCY_2_KEY, "USD") ?: "",
                prefs.getString(LIST_CURRENCIES_KEY, LIST_CURRENCIES) ?: ""
            )
        }

        /**
         * Deletes all user preferences from the shared preferences of the given [activity].
         *
         * @param activity the activity where the preferences are stored
         */
        fun deletePrefs(activity: Activity){
            activity.getSharedPreferences(PREF_PREFS, Context.MODE_PRIVATE).also { it.edit().clear().apply() }
        }

    }

}