package com.santa.ana.divisas.graphics

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.santa.ana.divisas.databinding.ActivityGraphicsBinding
import com.santa.ana.divisas.delimiter
import com.santa.ana.divisas.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import lecho.lib.hellocharts.gesture.ContainerScrollType
import lecho.lib.hellocharts.gesture.ZoomType
import lecho.lib.hellocharts.model.*
import java.util.*

/**
 * The [GraphicsActivity] displays a chart showing the exchange rate between two countries
 * for a selected range of dates. It receives the countries to display the exchange rate for
 * as an intent extra. It uses the [GraphicsViewModel] to fetch the exchange rate data from
 * an API and displays it using the [LineChartView] .
 */
@AndroidEntryPoint
class GraphicsActivity : AppCompatActivity() {

    /**
     * The [graphicsViewModel] is used to fetch the exchange rate data from an API.
     */
    private val graphicsViewModel : GraphicsViewModel by viewModels()
    /**
     * The [binding] is used to access the views in the activity_graphics layout file.
     */
    private lateinit var binding: ActivityGraphicsBinding
    /**
     * The [COUNTRIES_KEY] is used to retrieve the countries to display the exchange rate for
     * from the intent extra.
     */
    companion object{
        const val COUNTRIES_KEY = "countries"
    }

    /**
     * This function is called when the activity is created. It sets the content view,
     * retrieves the countries to display the exchange rate for from the intent extra,
     * fetches the exchange rate data using the [graphicsViewModel], and displays it using
     * the [LineChartView].
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Inflate the activity_graphics layout file using the ActivityGraphicsBinding class
        binding = ActivityGraphicsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve the countries to display the exchange rate for from the intent extra
        val countries = intent?.extras?.getString(COUNTRIES_KEY)
        // If no countries were specified, finish the activity
        if(countries == null){
            finish()
            return
        }else{
            Log.d("logDivisas", countries)
        }
        // Split the countries string into "from" and "to" currencies
        val from = countries.split(delimiter)[0]
        val to = countries.split(delimiter)[1]

        // Fetch the exchange rate data using the graphicsViewModel
        graphicsViewModel.getTimeSeries("2022-01-01..", from, to)

        // Observe the listTimeSeries LiveData and update the chart when it changes
        graphicsViewModel.listTimeSeries.observe(this){
            timeSeries ->
            // Configure the chart
            binding.chart.isInteractive = true
            binding.chart.zoomType = ZoomType.HORIZONTAL_AND_VERTICAL
            binding.chart.setContainerScrollEnabled(true, ContainerScrollType.HORIZONTAL)

            // Prepare the data for the chart
            val axisValues = mutableListOf<AxisValue>()
            val values = mutableListOf<PointValue>()
            val mapTimeSeries = timeSeries.rates
            val listKey = mapTimeSeries.keys.toList()

            val step = maxOf(1, listKey.size/5)

            //var i = 0
            var i = listKey.size/2 + listKey.size/3
            //while(i<listKey.size){
            for (i in listKey.indices step step){
                val mapValue = mapTimeSeries[listKey[i]]
                if(mapValue!=null){
                    Log.d("logDivisas", "key:"+listKey.elementAt(i)+" value:"+mapValue[to])

                    val axisValue = AxisValue(i.toFloat()).setLabel(listKey[i])
                    axisValues.add(axisValue)

                    if(mapValue[to]!=null){
                        values.add(PointValue(i.toFloat(), mapValue[to]!!.toFloat()))
                    }
                }
            }

            // Add the data to the chart
            val line = Line(values).setColor(Color.BLUE).setCubic(true)
            val lines: MutableList<Line> = ArrayList()
            lines.add(line)
            val data = LineChartData()
            val axisY: Axis = Axis().setHasLines(true)

            data.axisYLeft = axisY
            data.baseValue = Float.NEGATIVE_INFINITY
            data.axisXBottom = Axis(axisValues).setHasLines(true).setMaxLabelChars(5)
            data.lines = lines
            binding.chart.lineChartData = data
        }

        /**
         * When the button is clicked, it creates a new Intent. Finally, the startActivity() method
         * is called with the intent object to start the MainActivity.
         */
        binding.idFloatingBtnMain.setOnClickListener {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }

        /**
         * When the button is clicked, it creates a new Calendar object to get the current date and time.
         * It also creates a new instance of MyDatePicker class with the graphicsViewModel, from, and to variables as arguments.
         * Finally, it creates a new DatePickerDialog object and shows it on the screen.
         * The MyDatePicker object is passed as a listener to the DatePickerDialog, which is called when
         * the user selects a date from the dialog. The onDateSet() method of MyDatePicker class is called with the selected date as arguments.
         */
        binding.btnSelectDate.setOnClickListener {
            val calendar = Calendar.getInstance()
            val myDatePicker = MyDatePicker(graphicsViewModel, from, to)
            val datePickerDialog = DatePickerDialog(
                this,
                myDatePicker,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePickerDialog.show()
        }
    }

    /**
    * This class represents a date picker dialog that listens for date set events and updates a
    * GraphicsViewModel with a new time series based on the selected date.
    *
    * @property graphicsViewModel The GraphicsViewModel instance to update.
    * @property from The main currency.
    * @property to The secondary currency.
    */
    class MyDatePicker (private val graphicsViewModel: GraphicsViewModel, private val from: String, val to: String) : DatePickerDialog.OnDateSetListener{

        /**
         * Called when a date is set in the dialog.
         *
         * @param p0 The DatePicker instance.
         * @param year The selected year.
         * @param month The selected month, where January is 0.
         * @param dayOfMonth The selected day of the month, where 1 represents the first day.
         */
        override fun onDateSet(p0: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

            // Convert the month and day of month to 2-digit strings.
            val month1 = month + 1
            var month2:String = month.toString()
            var dayOfMonth1: String = dayOfMonth.toString()
            val year1: String = year.toString()
            if(month2.length==1){
                month2 = "0$month1"
            }
            if(dayOfMonth1.length==1){
                dayOfMonth1 = "0$dayOfMonth"
            }
            // Construct the date string in yyyy-MM-dd format.
            val date = "$year1-$month2-$dayOfMonth1.."
            // Update the GraphicsViewModel with the new time series.
            graphicsViewModel.getTimeSeries(date, from, to)
        }

    }
}