package com.santa.ana.divisas.graphics

import android.util.Log
import com.santa.ana.divisas.R
import com.santa.ana.divisas.api.ApiResponseStatus
import com.santa.ana.divisas.api.ApiService
import com.santa.ana.divisas.api.dto.TimeSeriesMapper
import com.santa.ana.divisas.api.makeNetworkCall
import com.santa.ana.divisas.model.TimeSeries
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * An interface that defines methods for fetching graphics data from an API.
 */
interface GraphicsTasks {

    /**
     *Fetches time series data from an API for a given time range.
     * @param time a string representing the time range to fetch data for.
     * @param from a string representing the main currency
     * @param to a string representing the secondary currency
     * @return an [ApiResponseStatus] object representing the result of the API call.
     */
    suspend fun getTimeSeries(time: String, from: String, to: String): ApiResponseStatus<TimeSeries>
}

/**
 * A repository class that implements the [GraphicsTasks] interface to fetch graphics data from an API.
 * @property apiService an instance of the [ApiService] interface used to make API calls.
 * @property dispatcher a [CoroutineDispatcher] instance used to switch coroutines to a different thread.
 */
class GraphicsRepository @Inject constructor(private val apiService: ApiService, private val dispatcher: CoroutineDispatcher) : GraphicsTasks {

    /**
     * Fetches time series data from an API for a given time range and wraps the result in an [ApiResponseStatus] object.
     * This method delegates the actual API call to [getTimeSeriesPrivate], which is wrapped in a [withContext] block to
     * switch to a different coroutine thread.
     * @param time a string representing the time range to fetch data for.
     * @param from a string representing the main currency
     * @param to a string representing the secondary currency
     * @return an [ApiResponseStatus] object representing the result of the API call.
     */
    override suspend fun getTimeSeries(
        time: String,
        from: String,
        to: String
    ): ApiResponseStatus<TimeSeries> {
        return withContext(dispatcher){
            when(val apiResponseStatus : ApiResponseStatus<TimeSeries> = getTimeSeriesPrivate(time, from, to)){
                is ApiResponseStatus.Error -> {
                    apiResponseStatus
                }
                is ApiResponseStatus.Success -> {
                    ApiResponseStatus.Success(apiResponseStatus.data)
                }
                else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }

    }

    /**
     * Makes a network call to fetch time series data from an API for a given time range and maps the response to a
     * [TimeSeries] domain model object.
     * @param time a string representing the time range to fetch data for.
     * @param from a string representing the main currency
     * @param to a string representing the secondary currency
     * @return an [ApiResponseStatus] object representing the result of the API call.
     */
    private suspend fun getTimeSeriesPrivate(time : String, from : String, to: String): ApiResponseStatus<TimeSeries> {
        return makeNetworkCall {
            val response = apiService.getTimeSeries(time, from, to)
            val timeSeriesMapper = TimeSeriesMapper()
            timeSeriesMapper.fromTimeSeriesResponseToTimeSeriesDomain(response)
        }
    }
}