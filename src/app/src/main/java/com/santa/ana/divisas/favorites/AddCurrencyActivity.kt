package com.santa.ana.divisas.favorites

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.santa.ana.divisas.LIST_CURRENCIES
import com.santa.ana.divisas.Preference
import com.santa.ana.divisas.R
import com.santa.ana.divisas.databinding.ActivityAddCurrencyBinding
import com.santa.ana.divisas.delimiter
import com.santa.ana.divisas.main.MainActivity
import com.santa.ana.divisas.main.listView.CurrencyAdapter
import com.santa.ana.divisas.model.Currency
import com.santa.ana.divisas.model.SignsCountries

/**
 * Activity for adding currencies.
 */
class AddCurrencyActivity : AppCompatActivity() {

    // View binding instance
    private lateinit var binding: ActivityAddCurrencyBinding

    companion object {
        const val COUNTRIES_KEY = "countries"
        const val AMOUNT_KEY = "amount"
        const val CURRENCIES_KEY = "currencies"
        const val MAP_KEY = "conversions"
    }

    /**
     * Activity's onCreate method.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddCurrencyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Retrieve data passed through the intent
        val countries = intent?.extras?.getString(COUNTRIES_KEY)
        val amount = intent?.extras?.getString(AMOUNT_KEY)
        var currencies = intent?.extras?.getString(CURRENCIES_KEY)
        val conversion: SignsCountries = intent?.extras?.get(MAP_KEY) as SignsCountries
        val setKeys = conversion.sign.keys.toMutableSet()
        val listAllCurrenciesString = mutableListOf<String>()

        // Check if required data is present, otherwise finish the activity
        if (countries == null || amount == null || currencies == null) {
            finish()
            return
        }

        // Convert currencies string to a list of Currency objects
        val sepCurrencies = currencies.split(delimiter)
        val listCurrenciesFavoritesCurrency = sepCurrencies.map { Currency(conversion.sign[it]!!, it, "") }.toMutableList()

        // Set the text in the AutoCompleteTextView
        binding.idAutoCompleteText1.setText(getString(R.string.country_currency_format, conversion.sign[countries.split(delimiter)[0]], countries.split(delimiter)[0]))

        // Build a list of all currencies for the AutoCompleteTextView
        listAllCurrenciesString.addAll(setKeys.map { getString(R.string.country_currency_format, conversion.sign[it], it) })

        // Set up the adapter for the AutoCompleteTextView
        val adapter = ArrayAdapter(applicationContext, R.layout.list_currencies_item, listAllCurrenciesString)
        binding.idAutoCompleteText1.setAdapter(adapter)

        // Set up the RecyclerView and its adapter
        val recycler = binding.currenciesRecycler
        recycler.layoutManager = GridLayoutManager(applicationContext, 1)
        val adapterRecycler = CurrencyAdapter()

        // Handle long click event in the RecyclerView adapter
        adapterRecycler.setOnLongItemClickListener { currency ->
            currencies = currencies?.split(delimiter)?.toMutableList()?.apply { remove(currency.nameCurrency) }?.joinToString(delimiter)

            val sepCurrenciesTemp = (currencies ?: LIST_CURRENCIES).split(delimiter)
            val listCurrenciesFavoritesCurrencyTemp = sepCurrenciesTemp.map { Currency(conversion.sign[it]!!, it, "") }.toMutableList()

            adapterRecycler.submitList(listCurrenciesFavoritesCurrencyTemp)

            savePreference(amount.split(delimiter)[0], amount.split(delimiter)[1], countries.split(delimiter)[0], countries.split(delimiter)[1], currencies ?: LIST_CURRENCIES)
        }

        // Handle click event in the RecyclerView adapter
        adapterRecycler.setOnItemClickListener { currency ->
            Toast.makeText(applicationContext, getString(R.string.country_currency_format, currency.nameCurrency, currency.name), Toast.LENGTH_SHORT).show()
        }

        recycler.adapter = adapterRecycler
        adapterRecycler.submitList(listCurrenciesFavoritesCurrency)

        // Add button click listener
        binding.btnAddCurrency.setOnClickListener {
            val signTextAuto = binding.idAutoCompleteText1.text.toString().split(delimiter)

            if (signTextAuto[1].trim() !in (currencies ?: LIST_CURRENCIES)) {
                currencies = (currencies ?: LIST_CURRENCIES) + delimiter + signTextAuto[1].trim()

                val sepCurrenciesTemp = (currencies ?: LIST_CURRENCIES).split(delimiter)
                val listCurrenciesFavoritesCurrencyTemp = sepCurrenciesTemp.map { Currency(conversion.sign[it]!!, it, "") }.toMutableList()

                adapterRecycler.submitList(listCurrenciesFavoritesCurrencyTemp)

                savePreference(amount.split(delimiter)[0], amount.split(delimiter)[1], countries.split(delimiter)[0], countries.split(delimiter)[1], currencies ?: LIST_CURRENCIES)
            } else {
                Log.d("logDivisas", "Currency already exists")
            }
        }

        // Floating button click listener
        binding.idFloatingBtnMain.setOnClickListener {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }
    }

    /**
     * Save user preferences.
     */
    private fun savePreference(amount1: String, amount2: String, currency1: String, currency2: String, listCurrencies: String) {
        Preference.setPreference(this, Preference(amount1, amount2, currency1, currency2, listCurrencies))
    }
}