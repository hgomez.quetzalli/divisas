package com.santa.ana.divisas.di

import com.santa.ana.divisas.graphics.GraphicsRepository
import com.santa.ana.divisas.graphics.GraphicsTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Dagger Hilt module that provides a way to inject a [GraphicsTasks] dependency into other classes.
 * The module is annotated with [@Module], indicating that it provides a set of dependencies that can be injected
 * into other classes. It is also annotated with [@InstallIn(SingletonComponent::class)], which specifies that the
 * dependencies provided by the module will be installed in the [SingletonComponent], a predefined component in
 * Dagger Hilt that provides a singleton instance of the dependencies. The module contains one abstract function
 * called bindGraphicsTasks that takes a [GraphicsRepository] object as a parameter and returns a [GraphicsTasks] object.
 * The [@Binds] annotation is used to tell Dagger Hilt that this function is responsible for providing an
 * implementation of the [GraphicsTasks] interface. When this module is installed in a Dagger Hilt component,
 * any class that requests a [GraphicsTasks] dependency will receive an instance of [GraphicsRepository]
 * through the bindGraphicsTasks function.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class GraphicsTasksModule {

    /**
     * Provides an implementation of the [GraphicsTasks] interface by binding a [GraphicsRepository] object.
     * @param graphicsRepository the [GraphicsRepository] object to be used as the implementation of the [GraphicsTasks] interface.
     * @return an instance of the [GraphicsTasks] interface that uses the provided [GraphicsRepository] object.
     */
    @Binds
    abstract fun bindGraphicsTasks(graphicsRepository: GraphicsRepository): GraphicsTasks

}