package com.santa.ana.divisas.main

import com.santa.ana.divisas.R
import com.santa.ana.divisas.api.ApiResponseStatus
import com.santa.ana.divisas.api.ApiService
import com.santa.ana.divisas.api.dto.ConversionMapper
import com.santa.ana.divisas.api.makeNetworkCall
import com.santa.ana.divisas.model.Conversion
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface MainTasks {

    suspend fun getAllCode(): ApiResponseStatus<Map<String, String>>
    suspend fun getConversion(amount : String, from : String): ApiResponseStatus<Conversion>
}

/**
 * we implement MainTask
 * We use Hilt for dependency injection, we inject ApiService and CoroutineDispatcher
 */
class MainRepository @Inject constructor(private val apiService: ApiService, private val dispatcher: CoroutineDispatcher) : MainTasks{

    /**
     * public function that request all country codes
     */
    override suspend fun getAllCode(): ApiResponseStatus<Map<String, String>> {
        return withContext(dispatcher){
            when (val allCode: ApiResponseStatus<Map<String, String>> = getAllCodePrivate()) {
                is ApiResponseStatus.Error -> {
                    allCode
                }
                is ApiResponseStatus.Success -> {
                    ApiResponseStatus.Success(allCode.data)
                }
                else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }
    }

    /**
     * Real function that request all country codes
     */
    private suspend fun getAllCodePrivate(): ApiResponseStatus<Map<String, String>> {
        return makeNetworkCall {
            val response = apiService.getAllCode()
            response
        }
    }

    /**
     * public function that request the conversion from the amount and currency in the parameters
     */
    override suspend fun getConversion(amount : String, from : String): ApiResponseStatus<Conversion> {
        return withContext(dispatcher){
            when(val conversion : ApiResponseStatus<Conversion> = getConversionPrivate(amount, from)){
                is ApiResponseStatus.Error -> {
                    conversion
                }
                is ApiResponseStatus.Success -> {
                    ApiResponseStatus.Success(conversion.data)
                }
                else -> {
                    ApiResponseStatus.Error(R.string.unknown_error)
                }
            }
        }
    }
    /**
     * Real function that request the conversion of an currency
     */
    private suspend fun getConversionPrivate(amount : String, from : String): ApiResponseStatus<Conversion>{
        return makeNetworkCall {
            val response = apiService.getConversion(amount, from)
            val conversionMapper = ConversionMapper()
            conversionMapper.fromConversionResponseToConversionDomain(response)
        }
    }

}