package com.santa.ana.divisas

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * base class for the Hilt-generated application component. With HiltAndroidApp we enable to Hilt
 * to generate the necessary code to set up the dependency injection framework for your entire application
 */
@HiltAndroidApp
class DivisasApplication: Application()