package com.santa.ana.divisas.main.listView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.santa.ana.divisas.R
import com.santa.ana.divisas.databinding.AdapterListCurrencyBinding
import com.santa.ana.divisas.model.Currency

/**
 * Class to create an adapter. This adapter is for list currency
 */
class CurrencyAdapter : ListAdapter<Currency, CurrencyAdapter.CurrencyViewHolder>(DiffCallback) {

    /**
     * class compare two lists and calculate the differences between them.
     * It is often used in conjunction with RecyclerView to efficiently update the UI when data changes.
     */
    companion object DiffCallback : DiffUtil.ItemCallback<Currency>() {

        override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
            return oldItem.nameCurrency == newItem.nameCurrency
        }

    }

    /**
     * we create a new viewHolder
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val binding = AdapterListCurrencyBinding.inflate(LayoutInflater.from(parent.context))
        return CurrencyViewHolder(binding)
    }

    /**
     * We binding the data to a viewHolder
     */
    override fun onBindViewHolder(currencyViewHolder: CurrencyViewHolder, position: Int) {
        val currency = getItem(position)
        currencyViewHolder.bind(currency)
    }

    /**
     * this property that is a function given from MainActivity
     */
    private var onItemClickListener: ((Currency) -> Unit)? = null
    fun setOnItemClickListener(onItemClickListener: (Currency) -> Unit){
        this.onItemClickListener = onItemClickListener
    }

    private var onLongItemClickListener: ((Currency) -> Unit)? = null
    fun setOnLongItemClickListener(onLongItemClickListener: (Currency) -> Unit){
        this.onLongItemClickListener = onLongItemClickListener
    }

    /**
     * ViewHolder that gets an adapterListCurrencyBinding to bind the currency data with the adapterListCurrency views
     */
    inner class CurrencyViewHolder(private val binding: AdapterListCurrencyBinding) : RecyclerView.ViewHolder(binding.root){

        /**
         * Function to bind currency data with the adapterListCurrency views
         */
        fun bind(currency: Currency){
            binding.nameTextView.text = binding.nameTextView.context.getString(R.string.country_currency_format, currency.name, currency.nameCurrency)
            binding.amountTextView.text = currency.amount
            binding.idConstraintLayout.setOnClickListener {
                onItemClickListener?.invoke(currency)
            }
            binding.idConstraintLayout.setOnLongClickListener {
                onLongItemClickListener?.invoke(currency)
                true
            }
        }
    }

}