package com.santa.ana.divisas.di

import com.santa.ana.divisas.main.MainRepository
import com.santa.ana.divisas.main.MainTasks
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * We use this abstract class to do dependency injection of the mainTask interface
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class MainTasksModule {
    @Binds
    abstract fun bindMainTasks(mainRepository: MainRepository): MainTasks
}