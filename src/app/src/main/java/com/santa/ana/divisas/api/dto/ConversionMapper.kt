package com.santa.ana.divisas.api.dto

import com.santa.ana.divisas.api.responses.ConversionResponse
import com.santa.ana.divisas.model.Conversion

class ConversionMapper {

    fun fromConversionResponseToConversionDomain(conversinoResponse: ConversionResponse): Conversion {
        return Conversion(conversinoResponse.amount, conversinoResponse.base, conversinoResponse.date, conversinoResponse.rates)
    }

}