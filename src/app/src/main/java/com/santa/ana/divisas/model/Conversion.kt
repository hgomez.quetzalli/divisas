package com.santa.ana.divisas.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Conversion  (
    val amount: Double,
val base: String,
val date: String,
val rates: Map<String, String>
): Parcelable