package com.santa.ana.divisas.api.responses

data class ConversionResponse(
    val amount: Double,
    val base: String,
    val date: String,
    val rates: Map<String, String>
)