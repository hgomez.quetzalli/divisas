package com.santa.ana.divisas.api

import android.util.Log
import com.santa.ana.divisas.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.net.UnknownHostException

private const val UNAUTHORIZED_ERROR_CODE = 401

/**
 * we use this function to give a generic response
 */
suspend fun <T> makeNetworkCall(call: suspend () -> T) : ApiResponseStatus<T> {

    return withContext(Dispatchers.IO){
        try {
            ApiResponseStatus.Success(call())
        }catch (e: UnknownHostException) {
            ApiResponseStatus.Error(R.string.unknown_host_exception_error)
        }catch (e: HttpException){
            val errorMessage = R.string.unknown_error
            ApiResponseStatus.Error(errorMessage)
        }catch (e : Exception){
            Log.e("logDivisas", e.message+"")
            val message = R.string.unknown_error
            ApiResponseStatus.Error(message)
        }
    }
}