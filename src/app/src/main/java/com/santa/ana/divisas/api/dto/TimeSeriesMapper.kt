package com.santa.ana.divisas.api.dto

import com.santa.ana.divisas.api.responses.TimeSeriesResponse
import com.santa.ana.divisas.model.TimeSeries

class TimeSeriesMapper {

    fun fromTimeSeriesResponseToTimeSeriesDomain(timeSeriesResponse: TimeSeriesResponse): TimeSeries {
        return TimeSeries(timeSeriesResponse.amount, timeSeriesResponse.base, timeSeriesResponse.start_date, timeSeriesResponse.end_date, timeSeriesResponse.rates)
    }

}